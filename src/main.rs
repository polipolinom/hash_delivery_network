#![forbid(unsafe_code)]

mod requests;
mod server;
mod structs;
mod utils;

use std::net::{IpAddr, Ipv4Addr};

use crate::server::run_server::run;

fn main() {
    let localhost_v4 = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
    run(localhost_v4, 7777);
}
