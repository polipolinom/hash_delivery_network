#![forbid(unsafe_code)]

use crate::requests::load;
use crate::requests::store;
use crate::structs::request::ClientRequest;
use crate::utils::readers::read_client_name;
use crate::utils::readers::read_client_request;
use crate::utils::senders::send_find_key;
use crate::utils::senders::send_good_insert;
use crate::utils::senders::send_no_key;

use std::net::Shutdown;
use std::{
    collections::HashMap,
    net::{IpAddr, SocketAddr, TcpListener, TcpStream},
    sync::{Arc, Mutex},
    thread,
};

type PointerStorage = Arc<Mutex<HashMap<String, String>>>;

fn handle_client(mut stream: TcpStream, storage: PointerStorage) {
    thread::spawn(move || {
        match read_client_name(&mut stream) {
            Err(_e) => {
                /*Print log with error*/
                stream.shutdown(Shutdown::Both).unwrap();
                return;
            }
            _ => {}
        }

        /*Log: Connection established*/

        loop {
            let action = match read_client_request(&mut stream) {
                Err(_e) => {
                    /*Print log with error*/
                    stream.shutdown(Shutdown::Both).unwrap();
                    return;
                }
                Ok(request) => request,
            };

            match action {
                ClientRequest::Load { key } => {
                    let mut guard_storage = storage.lock().unwrap();
                    let answer = load::load(&mut guard_storage, &key);
                    drop(guard_storage);

                    match answer {
                        None => {
                            if send_no_key(&mut stream).is_err() {
                                /*log that client is disconnect*/
                                stream.shutdown(Shutdown::Both).unwrap();
                                return;
                            }
                            /*log with no key*/
                            continue;
                        }

                        Some(hash) => {
                            if send_find_key(&mut stream, &key, &hash).is_err() {
                                /*log that client is disconnect*/
                                stream.shutdown(Shutdown::Both).unwrap();
                                return;
                            }
                            /*log find key*/
                            continue;
                        }
                    }
                }
                ClientRequest::Store { key, hash } => {
                    let mut guard_storage = storage.lock().unwrap();
                    store::store(&mut guard_storage, &key, &hash);
                    drop(guard_storage);

                    if send_good_insert(&mut stream).is_err() {
                        /*log that client is disconnect*/
                        stream.shutdown(Shutdown::Both).unwrap();
                        return;
                    }
                    /*log success insert*/
                    continue;
                }
            }
        }
    });
}

fn listen(listener: &TcpListener) {
    let storage: PointerStorage = Arc::new(Mutex::new(HashMap::new()));

    for stream_result in listener.incoming() {
        if stream_result.is_err() {
            continue;
        }

        let stream = stream_result.unwrap();
        handle_client(stream, storage.clone());
    }
}

pub fn run(ip: IpAddr, port: u16) {
    let addr = SocketAddr::new(ip, port);
    let listener = TcpListener::bind(addr).unwrap();

    listen(&listener);
    drop(listener);
}
