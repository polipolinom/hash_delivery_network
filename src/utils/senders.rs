#![forbid(unsafe_code)]

use std::{
    io::{self, Write},
    net::TcpStream,
};

pub fn send_no_key(stream: &mut TcpStream) -> io::Result<()> {
    let msg = "{\n
        \"response_status\": \"key not found\", \n
    }\n";
    stream.write_all(msg.as_bytes())
}

pub fn send_good_insert(stream: &mut TcpStream) -> io::Result<()> {
    let msg = "{\n
        \"response_status\": \"success\", \n
    }\n";
    stream.write_all(msg.as_bytes())
}

pub fn send_find_key(stream: &mut TcpStream, key: &String, hash: &String) -> io::Result<()> {
    let msg = format!(
        "{{ \n
        \"response_status\": \"success\", \n
        \"requested_key\": \"{}\", \n
        \"requested_hash\": \"{}\", \n
      }}\n",
        key, hash
    );
    stream.write_all(msg.as_bytes())
}
