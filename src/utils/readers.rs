#![forbid(unsafe_code)]

use serde_json::json;
use std::io::{BufRead, BufReader};
use std::net::TcpStream;

use crate::structs::request::ClientRequest;

use super::parse::{get_client_name, get_load_request, get_store_request};

fn read_json(stream: &mut TcpStream) -> Result<serde_json::Value, &'static str> {
    let mut reader = BufReader::new(stream);

    let mut buffer = vec![];
    if reader.read_until(b'}', &mut buffer).is_err() {
        return Err("Error during read_until");
    };
    let json_string = format!("{:?}", &buffer);
    let json = match serde_json::from_str(&json_string) {
        Ok(j) => j,
        Err(_e) => {
            return Err("It is not json");
        }
    };
    Ok(json!(json))
}

pub fn read_client_name(stream: &mut TcpStream) -> Result<String, &'static str> {
    let msg = read_json(stream)?;
    get_client_name(msg)
}

pub fn read_client_request(stream: &mut TcpStream) -> Result<ClientRequest, &'static str> {
    let msg = read_json(stream)?;
    match msg {
        serde_json::Value::Object(map) => {
            let request_type = match map.get("request_type") {
                None => {
                    return Err("Uncorrect format of request");
                }
                Some(value) => {
                    if !value.is_string() {
                        return Err("Request type must be a string");
                    }
                    value.as_str().unwrap()
                }
            };

            if request_type != "load" && request_type != "store" {
                return Err("Uncorrect type of request");
            }

            if request_type == "load" {
                return get_load_request(&map);
            }

            get_store_request(&map)
        }
        _ => {
            return Err("Uncorrect format of request");
        }
    }
}
