#![forbid(unsafe_code)]

use crate::structs::request::ClientRequest;
use serde_json::Map;

pub fn get_client_name(msg: serde_json::Value) -> Result<String, &'static str> {
    match msg {
        serde_json::Value::Object(map) => match map.get("student_name") {
            None => {
                return Err("First request haven't field student_name");
            }
            Some(value) => {
                if !value.is_string() {
                    return Err("First request haven't name in field student_name");
                }
                if map.len() != 1 {
                    return Err("First request contatins not only name");
                }
                return Ok(value.as_str().unwrap().to_string());
            }
        },
        _ => {
            return Err("First request isn't correct json with name");
        }
    };
}

pub fn get_load_request(
    map: &Map<String, serde_json::Value>,
) -> Result<ClientRequest, &'static str> {
    match map.get("key") {
        None => return Err("Load request must contain field 'key'"),
        Some(value) => {
            if !value.is_string() {
                return Err("Key must be a string");
            }
            if map.len() != 2 {
                return Err("Load request must contain only two fileds - 'request_type' and 'key'");
            }
            return Ok(ClientRequest::Load {
                key: value.as_str().unwrap().to_string(),
            });
        }
    }
}

pub fn get_store_request(
    map: &Map<String, serde_json::Value>,
) -> Result<ClientRequest, &'static str> {
    let key = match map.get("key") {
        None => return Err("Store request must contain field 'key'"),
        Some(value) => {
            if !value.is_string() {
                return Err("Key must be a string");
            }
            value.as_str().unwrap()
        }
    };

    let hash = match map.get("hash") {
        None => return Err("Store request must contain field 'hash'"),
        Some(value) => {
            if !value.is_string() {
                return Err("Hash must be a string");
            }
            value.as_str().unwrap()
        }
    };

    Ok(ClientRequest::Store {
        key: key.to_string(),
        hash: hash.to_string(),
    })
}
