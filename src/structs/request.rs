#![forbid(unsafe_code)]

pub enum ClientRequest {
    Store { key: String, hash: String },
    Load { key: String },
}
