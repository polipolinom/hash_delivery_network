#![forbid(unsafe_code)]

use std::collections::HashMap;

pub fn store(storage: &mut HashMap<String, String>, key: &str, hash: &str) {
    storage.insert(key.to_string(), hash.to_string());
}
