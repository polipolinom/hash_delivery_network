#![forbid(unsafe_code)]

use std::collections::HashMap;

pub fn load(storage: &mut HashMap<String, String>, key: &str) -> Option<String> {
    storage.get(key).map(|x| x.clone())
}
